using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pushableScript : MonoBehaviour
{
    [SerializeField]
    public int id = 1;
    EventManager GM;
    // Start is called before the first frame update
    void Awake()
    {
        GM = EventManager.Instance;
        GM.onFreezeObject -= FreezeMe;
        GM.onUnFreezeObject -= UnFreezeMe;
        GM.onFreezeObject += FreezeMe;
        GM.onUnFreezeObject += UnFreezeMe;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
     
    public void FreezeMe(int instanceID) //this prevents the object from being pushable
    {
        if (instanceID == gameObject.GetInstanceID())
        {
            var rb = GetComponent<Rigidbody2D>();
            print("Freeze");
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            
        }
    }
    public void UnFreezeMe(int instanceID) //this allows the  object to be pushable again
    {
        
        if (instanceID == gameObject.GetInstanceID())
        {
            var rb = GetComponent<Rigidbody2D>();
            rb.constraints = RigidbodyConstraints2D.None;
            rb.freezeRotation = true;

        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

            if (collision.gameObject.GetComponent<PlayerController>() != null)
            {
                GM.PushTriggerEnter(id, gameObject.GetInstanceID());
                //send to gamemanager the id 
            }
        else
        {
            //FreezeMe(gameObject.GetInstanceID());
        }
            if (collision.gameObject.GetComponent<ForageController>() != null)
            {
            UnFreezeMe(gameObject.GetInstanceID());
            //send to Gamemanager 
        }

    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<PlayerController>() != null)
        {
           if ((Input.GetAxis("Vertical") ==0 && GetComponent<Rigidbody2D>().velocity.x ==0) && (Input.GetAxis("Horizontal") == 0 && GetComponent<Rigidbody2D>().velocity.y == 0))
            {
                
                   collisionExit(collision, "staying");
                
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collisionExit(collision, "no");
    }
    private void collisionExit(Collision2D collision, string stay)
    {
        if (collision.gameObject.GetComponent<PlayerController>() != null)
        {
            GM.PushTriggerExit(id, gameObject.GetInstanceID(), stay);
            //send to gamemanager the id 
        }
        if (collision.gameObject.GetComponent<ForageController>() != null)
        {
            //send to Gamemanager 
        }
    }

}
