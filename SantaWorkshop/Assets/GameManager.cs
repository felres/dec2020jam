using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GameState { INTRO, MAIN_MENU, LEVEL_SELECT, GAME, CREDITS } //different states of the game

public delegate void OnStateChangeHandler();



public class GameManager : MonoBehaviour
{
    protected GameManager()
    {
    }
    private static GameManager instance = null;
    public event OnStateChangeHandler OnStateChange;

    public GameState gameState { get; private set; }
    public int currLevel = 0;//current level
    public int[] levelSize = new int[] {8,5,6,7 }; //based on order in list, sets length of array for each level
    public int gingerbreadCount = 0;
    // make sure the constructor is private, so it can only be instantiated here
    private void Awake()
    {

        if (instance== null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void incrementGingerbreadCount()
    {
        gingerbreadCount++;
        printGingerbreadCount();
    }

    public void decreaseGingerbreadCount()
    {
        gingerbreadCount--;
        printGingerbreadCount();
    }

    private void printGingerbreadCount()
    {
        print("Population amount: " + gingerbreadCount);
    }

    public static GameManager Instance
    {
        get
        {
            if (GameManager.instance == null)
            {
                GameManager.instance = new GameManager();

            }
            return GameManager.instance;
        }

    }



    public void SetGameState(GameState state)
    {
        this.gameState = state;
        OnStateChange();
    }

 
    public void SetCurrLevel(int level)
    {
        this.currLevel = level;
    }
    public void OnApplicationQuit()
    {
        GameManager.instance = null;
    }

    public static bool within(float a, float b, float within )
    {
        return Mathf.Abs(a - b) <= within;
    }
    public static float FindDegree(float x, float y)
    {
        float value = (float)((System.Math.Atan2(x, y) / System.Math.PI) * 180);
        if (value < 0) value += 360f;
        return value;
    }
    public static float DirectionToAngle(Vector2 input)
    {

        return Vector2.SignedAngle(Vector2.right, input);
    }

    public static int AngleToCardinal(float input)
    {
        //returns 0 to 3 based on signed angle of direction moving
        return 0;
    }
    public static int AngleToOrthogonal(float input)
    {
        //returns 0 to 7 based on signed angle of direction moving
        return 0;
    }
    public static float AngleTo( Vector2 from, Vector2 to)
    {
        Vector2 direction = to - from;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        if (angle < 0f) angle += 360f;
        return angle;
    }

    public static float map(float value, float start1, float stop1, float start2, float stop2  )
    {
        //value Number: the incoming value to be converted
        //start1 Number: lower bound of the value's current range
        //stop1 Number: upper bound of the value's current range
        //start2 Number: lower bound of the value's target range
        //stop2 Number: upper bound of the value's target range
        //Re-maps a number from one range to another.
        float returnvalue = 0;
        returnvalue=((stop2 - start2) * value) / (stop1 - start1);


        return returnvalue;

    }



}



