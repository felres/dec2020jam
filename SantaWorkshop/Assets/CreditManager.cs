using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditManager : MonoBehaviour
{
    public UIManager ui;
    public GameObject credit;
    public bool ena = false;
    public bool df = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetCredit()
    {
        ena = !ena;
        credit.SetActive(ena);
        if (df == true)
        {
            Invoke("HandlePs", .1f);
        }
    }
    void HandlePs()
    {
        //ui.HandlePauseCommand();
    }
    public void OffCredit()
    {
        ena = false;
        credit.SetActive(ena);
    }
}
