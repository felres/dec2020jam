using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvenAnimate : MonoBehaviour
{
    public Animator Oven;
    public Animator OvenShade;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ChangeAnimation(int change)
    {
        Oven.Play("oven_GBM_"+change);
    }

}
