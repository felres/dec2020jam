using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class instructionTurnOff : MonoBehaviour
{
    public GameObject a;
    public GameObject b;
    public float turnoffspeed = 8f;
    // Start is called before the first frame update
    void Awake()
    {
        Invoke("TurnOff", turnoffspeed);
        Invoke("Advice1", 20f);
        Invoke("Advice2", 40f);

    }
    void TurnOff()
    {
        a.SetActive(false);
        b.SetActive(false);
    }
    void Advice2()
    {
        a.SetActive(true);
        b.SetActive(true);
        a.GetComponent<Text>().text ="When Gingerbread men are carrying dough, they follow frosting and leave behind dough drips. When  they aren't, they follow dough drips and leave behind frosting.";
        Invoke("TurnOff", turnoffspeed);

    }
    void Advice1()
    {
        a.SetActive(true);
        b.SetActive(true);
        a.GetComponent<Text>().text = "Santa can press Z to lick up frosting and dough left behind by Gingerbread men, which gives back stamina. Stamina gets used when Santa pushes something heavy. ";
        Invoke("TurnOff", turnoffspeed);

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
