using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;

public class Intro : MonoBehaviour
{

    GameManager GM;

    void Awake()
    {
        GM = GameManager.Instance;
        print(GM);
        GM.OnStateChange += HandleOnStateChange;

        Debug.Log("Current game state when Awakes: " + GM.gameState);
    }

    void Start()
    {
        Debug.Log("Current game state when Starts: " + GM.gameState);
        GM.SetGameState(GameState.MAIN_MENU);

    }

    private void HandleOnStateChange()
    {
        Debug.Log("Handling state change to: " + GM.gameState + ", changing in 3 seconds");
        Invoke("LoadLevel", 3f); //delays by 3 seconds
    }

    private void LoadLevel()
    {
        print("OK");
        SceneManager.LoadScene("menu");
    }


}