using System.Collections.Generic;
using UnityEngine;

public class ForageController : MonoBehaviour
{
    // Read only
    [SerializeField]
    private float maxSpeed = 1;
    [SerializeField]
    private float steerStrength = 2;
    [SerializeField]
    private float crumbSteerStrength = 0.6f;
    [SerializeField]
    private float frostingSteerStrength = 0.1f;
    [SerializeField]
    private float wallSteerStrength = 0.00f;
    [SerializeField]
    private float wanderStrength = 0.05f;    
    [SerializeField]
    private float viewRadius = 1;
    [SerializeField]
    private float SpiderSenseViewRadius = 0.3f;
    [SerializeField]
    private float viewAngle = 90;
    [SerializeField]
    private string foodLayerName = "Food";
    [SerializeField]
    private string crumbLayerName = "Crumbs";
    [SerializeField]
    private string frostingLayerName = "Frosting";
    [SerializeField]
    private string wallLayerName = "Solid";
    [SerializeField]
    private GameObject crumb;
    [SerializeField]
    private GameObject frosting;
    [SerializeField]
    private float maxLifespan = 10f;
    public float lifespan;
    [SerializeField]
    public GameObject doughObject;
    [SerializeField]
    public float trailRate = 2;
    [SerializeField]
    private float timer = 0;
    public bool suckLifeTime = false;

    // These values are being modified
    /**
     * State id:
     * 0: Roam around
     * 1: Go back to nest
     */
    public int state;
    private float angle;
    float alarmRate = 2;
    private LayerMask foodLayer;
    private LayerMask crumbLayer;
    private LayerMask frostingLayer;
    private int wallLayer;
    Vector2 velocity;
    Vector2 desiredDirection;
    Transform targetFood;
    EventManager GM;
    private Rigidbody2D rb;
    private GameManager gm;
    public GameObject my_oven;
    public float multiplier = 1;
     float startStop = 1;

    public Vector2 nest;


    // Start is called before the first frame update
    void Awake()
    {
        lifespan = maxLifespan;
        foodLayer = LayerMask.GetMask(foodLayerName);
        crumbLayer = LayerMask.GetMask(crumbLayerName);
        wallLayer = LayerMask.GetMask(wallLayerName);
        frostingLayer = LayerMask.GetMask(frostingLayerName);
        rb = GetComponent<Rigidbody2D>();
        GM = EventManager.Instance;
        print("Gingerbread is born");

        gm = GameManager.Instance;
        GM.onReset += Die;
        gm.incrementGingerbreadCount();
    }

    void Update()
    {
        lifespan -= Time.deltaTime;
        if (suckLifeTime == true)
        {
            SuckLife();
        }
        if (lifespan <= 0)
        {
            Die();
        }

    }
    public void SuckLife()
    {
        //drains life from gingerbread man

            lifespan -= Time.deltaTime*2;


        
    }
    private void Die()
    {
        if (state == 1)
            Instantiate(doughObject, transform.position, transform.rotation);
        GM.MainTextChange("A Gingerbread Man Crumbled into Cookie Dust...");

        GM.ChangeGBMCount(-1);

       // gm.incrementGingerbreadCount();
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // A randomized general direction
        if (state == 0)
            desiredDirection = (desiredDirection + Random.insideUnitCircle * wanderStrength).normalized;
        else
            desiredDirection = (desiredDirection).normalized;

        // Get acceleration based on desired values
        Vector2 desiredVelocity = desiredDirection * maxSpeed*multiplier*startStop;
        Vector2 desiredSteeringForce = (desiredVelocity - velocity) * steerStrength;
        Vector2 acceleration = Vector2.ClampMagnitude(desiredSteeringForce, steerStrength) / 1;

        // Set velocity
        velocity = Vector2.ClampMagnitude(velocity + acceleration * Time.fixedDeltaTime, maxSpeed*multiplier*startStop);
        angle = Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;


        //rb.AddForce((velocity) );
        rb.MovePosition((Vector2)transform.position + (velocity * Time.fixedDeltaTime));
        rb.MoveRotation(angle);

        GetComponent<gingerbreadAnimate>().ChangeAngle(desiredDirection);
        
        if (state == 0) // Explore
        {
            HandleFood();
            leaveTrail(frosting);
        }
        else if (state == 1) // Go back to nest
        {
            ReturnToNest();

            leaveTrail(crumb);
        }

        AvoidWalls();



    }

    

    private void ReturnToNest()
    {
        //desiredDirection = (nest - (Vector2)transform.position).normalized;
        SteerTowardsObjectInLayer(frostingLayer, frostingSteerStrength);
        
        float minDist = 0.8f;
        if (Vector2.Distance(nest, transform.position) < viewRadius)
        {
            desiredDirection = nest - (Vector2)transform.position;
            // go back to roaming state
            if (Vector2.Distance(nest, transform.position) < minDist)
            {
                my_oven.GetComponent<OvenBehavior>().increaseDoughCount();
                // desiredDirection *= -1;
                //startStop = 0;
                GM.ChangeCrumbCount(-1);
                state = 0;
                Instantiate(crumb, transform.position, frosting.transform.rotation);

                GetComponent<gingerbreadAnimate>().ChangeState(state);

            }
        }
    }

    private void leaveTrail(GameObject thiscrumb)
    {
        float withinF = Time.fixedDeltaTime;
        if (GameManager.within((Time.time % trailRate), 0, withinF) && GM.getCrumbCount() <GM.getMaxCrumbAmount())
        {
            GameObject crumbCurrent = Instantiate(thiscrumb, transform.position, thiscrumb.transform.rotation);
            crumbCurrent.GetComponent<Crumb>().facingAngle = angle;
            GM.ChangeCrumbCount(1);
        }
    }

    void HandleFood()
    {
        // If target food doesnt exist
        if (targetFood == null)
        {
            // Get listo of all Food objects within perception radius
            Collider2D[] allFood = Physics2D.OverlapCircleAll(transform.position, viewRadius, foodLayer);

            if (allFood.Length > 0)
            { 
                // Choose one food at random
                Transform food = allFood[Random.Range(0, allFood.Length)].transform;
                // Get relative direction from my position to food position
                Vector2 dirToFood = (food.position - transform.position).normalized;

                Debug.DrawLine(transform.position, (Vector2)transform.position + dirToFood, Color.green);

                if (Vector2.Angle(transform.right, dirToFood) < viewAngle / 2)
                {
                    //food.gameObject.layer = Layers.takenFoodLayer;
                    targetFood = food;
                }
            }
            else
            {
                SteerTowardsObjectInLayer(crumbLayer, crumbSteerStrength);
            }
        }
        else
        {
            // Try to move towards target food
            desiredDirection = (targetFood.position - transform.position).normalized;

            // Pick up the food if it is close enough
            const float foodPickupRadius = 1f;
            if (Vector2.Distance(targetFood.position, transform.position) < foodPickupRadius)
            {
                Destroy(targetFood.gameObject);
                targetFood = null;
                //                desiredDirection *= -1;
                //transform.up = -desiredDirection;
                Instantiate(frosting, transform.position, frosting.transform.rotation);
                state = 1;
                GetComponent<gingerbreadAnimate>().ChangeState(state);

            }
        }
    }

    private void SteerTowardsObjectInLayer(int layerToCheck, float thisSteerStrength)
    {
        Collider2D[] crumbArray = Physics2D.OverlapCircleAll(transform.position, viewRadius, layerToCheck);
        List<GameObject> leftCrumbs = new List<GameObject>(); float leftValue=0;
        List<GameObject> centerCrumbs = new List<GameObject>(); float centerValue = 0;
        List<GameObject> rightCrumbs = new List<GameObject>(); float rightValue = 0;

        for ( int i = 0; i < crumbArray.Length; i++)
        {
            Vector2 dirToCrumb = (crumbArray[i].transform.position - transform.position).normalized;

            // get the signed difference from the directionim facing and the actual 
            float angDif = Vector2.SignedAngle(transform.right, dirToCrumb);

            if( (Mathf.Abs(angDif) < viewAngle / 2)||
                (Vector2.Distance(crumbArray[i].transform.position, transform.position) <= SpiderSenseViewRadius))
            {
                
                if (Mathf.Abs(angDif) < viewAngle / 6)
                {
                    Debug.DrawLine(transform.position, (Vector2)transform.position + dirToCrumb, Color.grey);
                    centerCrumbs.Add(crumbArray[i].gameObject);
                }
                else if (angDif < 0)
                {
                    Debug.DrawLine(transform.position, (Vector2)transform.position + dirToCrumb, Color.blue);
                    leftCrumbs.Add(crumbArray[i].gameObject);
                }
                else
                {
                    Debug.DrawLine(transform.position, (Vector2)transform.position + dirToCrumb, Color.red);
                    rightCrumbs.Add(crumbArray[i].gameObject);
                }
            }
        }

        // get values
        for (int i = 0; i < leftCrumbs.Count; i++)
            leftValue += leftCrumbs[i].GetComponent<Crumb>().getValue();
        for (int i = 0; i < centerCrumbs.Count; i++)
            centerValue += centerCrumbs[i].GetComponent<Crumb>().getValue();
        for (int i = 0; i < rightCrumbs.Count; i++)
            rightValue += rightCrumbs[i].GetComponent<Crumb>().getValue();

        float closeness = 1f;
        Collider2D randomCrumb = Physics2D.OverlapCircle(transform.position, viewRadius, layerToCheck);
        if (randomCrumb != null)
            closeness = (Vector2.Distance(randomCrumb.transform.position, (Vector2)transform.position) / viewRadius);

        if (centerValue > Mathf.Max(leftValue, rightValue))
        {
            //print("center!");

            desiredDirection = transform.right;
        }
        else if (leftValue > rightValue)
        {
            // print("left!");

            desiredDirection = transform.right + (transform.up * thisSteerStrength * closeness * multiplier*startStop);
        }
        else if (rightValue > leftValue)
        {

            // print("right!"); 
            desiredDirection = transform.right - (transform.up * thisSteerStrength * closeness * multiplier*startStop);
        }

        else
        {

        }
        //desiredDirection = crumb.transform.rotation;
    }

    private void AvoidWalls()
    {
        Collider2D[] walls = Physics2D.OverlapCircleAll(transform.position, viewRadius, wallLayer);
        
        Collider2D wall = null;
        if (walls.Length > 0) wall = walls[0];
        // Get closest wall for better steering
        /*float closestDistance = viewRadius;
        for (int i = 0; i < walls.Length; i++)
        {
            float currentDistance = Vector2.Distance(walls[i].ClosestPoint(transform.position), (Vector2)transform.position);
            print("1");
            if (closestDistance > currentDistance)
            {
                print("2");
                wall = walls[i];
                closestDistance = currentDistance;

            }
        }*/
        if ((wall != null) && (targetFood == null))
        {
            Vector2 dirToWall = (wall.ClosestPoint(transform.position) - (Vector2)transform.position).normalized;
            Debug.DrawLine(transform.position, (Vector2)transform.position + dirToWall, Color.blue);
            // get the signed difference from the directionim facing and the actual 
            float angDif = Vector2.SignedAngle(transform.right, dirToWall);
            float closeness = 1 - (Vector2.Distance(wall.ClosestPoint(transform.position), (Vector2)transform.position) / viewRadius);
            if (angDif < 0)
                desiredDirection += (Vector2)(transform.up * wallSteerStrength * closeness);
            else
                desiredDirection += -(Vector2)(transform.up * wallSteerStrength * closeness);

            //desiredDirection = -transform.up;
        }
    }
    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, viewRadius);
        
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, SpiderSenseViewRadius);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, (Vector2)transform.position + desiredDirection);
    }
}
