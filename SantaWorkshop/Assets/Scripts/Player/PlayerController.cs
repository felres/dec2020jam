using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    float speed = 1;
    [SerializeField]
    float stamina = 1;
    [SerializeField]
    float caloriesPerCrumb = .005f;
    bool staminaLower = false;
    int currentlyPushing;
    float staminaLowerBy = .1f;
    float newSpeed = 1;
    public soundManager sm;
    float translationY;
    float translationX;
    bool pressed;
    bool performing;
    EventManager GM;
    [SerializeField]
    float contextRadius = 1;
    [SerializeField]
    float strength = 4;
    Rigidbody2D rb;
    GameObject performingItem;
    // Start is called before the first frame update
    bool sucking = false;
    [SerializeField]
    public GameObject eating;
    [SerializeField]
     GameObject sweat;
    string lastDir = "forward";
    bool sweating = false;
    GameObject sweatObject;
    bool full = false;
    [SerializeField]
     float fullStomach = 0;
    [SerializeField]
    float fullStomachLower = 0.01f;
    int currentID = 0;
    bool shove = false;
    bool licking = false;
    private void Awake()
    {
        GM = EventManager.Instance;

        GM.OnPlayerPusher += OnPlayerPushing;
        GM.onStaminaIncrease += OnStaminaIncrease;

        GM.onPlayerExit += OnPlayerExit;
        GM.onReset += Reset;

        rb = GetComponent<Rigidbody2D>();
        newSpeed = speed;
    }
    public void SetShove(bool setter)
    {
        shove = setter;
    }
    public void Reset()
    {
        licking = false;
        shove = false;
        currentID = 0;
        fullStomach = 0;
        currentlyPushing = 0;
        pressed = false;
    }
    // Update is called once per frame
    private void Update()
    {
         translationY = Input.GetAxis("Vertical") * speed;
         translationX = Input.GetAxis("Horizontal") * speed;
        bool isPressed = Input.GetKeyDown(KeyCode.Z);
        bool endPressed = Input.GetKeyUp(KeyCode.Z);
        if (isPressed)
        {
            pressed = true;
        }
        if (endPressed)
        {
            checkReactivate();
            licking = false;
            pressed = false;
        }
        if (licking == true)
        {
            sm.PlayMe();
        }
        else
        {
            sm.StopMe();
        }
        if (pressed == true)
        {
            contextTrigger();
        }

        string dir = "idle";
        if (translationX < 0)
        {
            lastDir = "left";
            dir = "left";
        }
        else if (translationX > 0)
        {
            lastDir = "right";
            dir = "right";
        }

        else if (translationY< 0)
        {
            lastDir = "forward";
            dir = "forward";
        }
        else if (translationY > 0)
        {
            lastDir = "up";
            dir = "up";
        }
        if (staminaLower == true)
        {
            staminaLow();

        }
        if (fullStomach >0)
        {
            fullStomach -= fullStomachLower * Time.deltaTime;
            GM.FoodChange(fullStomach);
        }
        if (currentlyPushing == 0)
        {
            sweating = false;
            if (Mathf.RoundToInt((Time.time * 10)) % 8 == 0 || dir == "idle")
            {
                GetComponent<SantaAnimate>().ChangeAnimation(dir, false, false, shove, licking, lastDir);
            }

        }
        else
        {
             
                GetComponent<SantaAnimate>().ChangeAnimation(dir, true, false, shove, licking, lastDir);
           

           
        }
       // sweatObject.SetActive(true);
    }
    void contextTrigger()
    {
        if (performing == false) //is performing false? This means that the action has been completed
        {
            Collider2D[] contextList = Physics2D.OverlapCircleAll(transform.position, contextRadius);
            float closestDistance = contextRadius;
            GameObject closestItem = null; 
            for (int i = 0; i < contextList.Length; i++)
            {
                if (contextList[i].gameObject.GetComponent<PlayerController>() ==null)
                {
                    float currentDistance = Vector2.Distance(transform.position, contextList[i].transform.position);
                    if (closestDistance > currentDistance)
                    {
                        closestItem = contextList[i].gameObject;
                        closestDistance = currentDistance;

                    }
                }
            }
            //set to performingitem
            if (closestItem != null)
            {
                performingItem = closestItem;
            }
            if (performingItem != null)
            {
                if (performingItem.GetComponent<Crumb>() != null)
                {
//                    print("crumb");
                    if (fullStomach<1)
                    {
                        licking = true;
                        Destroy(performingItem.gameObject);
                        fullStomach += caloriesPerCrumb * Time.deltaTime;
                        if (stamina < 1)
                        {
                            stamina += caloriesPerCrumb * Time.deltaTime;
                        }
                        GM.ChangeCrumbCount(-1);

                        GM.StaminaChange(stamina);
                    }
                    else
                    {
                        licking = false;
                    }

                    //send destroyObject to eventmanager 
                }
                /*

                    else if (performingItem.GetComponent<pushableScript>() != null)
                    {
                        if (stamina > 0)
                        {

                            if (stamina - .3f > 0)
                            {
                                stamina -= .3f;
                                shove = true;
                                //  print("Push");
                                if (performingItem.GetComponent<pushableScript>().id == 1)
                                {
                                    performingItem.GetComponent<Rigidbody2D>().AddForce(-(transform.position - performingItem.transform.position).normalized * strength, ForceMode2D.Impulse);
                                }
                                else
                                {
                                    print("gingu");
                                    performingItem.GetComponent<Rigidbody2D>().AddForce(-(transform.position - performingItem.transform.position).normalized * strength * 10, ForceMode2D.Impulse);

                                }
                            }
                            else
                            {
                                stamina = 0;
                            }
                            GM.StaminaChange(stamina);
                            pressed = false;
                        }

                        //send push to eventmanager at direction
                        //sets performing to false
                    }
                    */
                    /*
                    else if (performingItem.GetComponent<ForageController>() != null)
                    {
                        //print("forager");

                        performingItem.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                        eating.SetActive(true);
                        performingItem.GetComponent<gingerbreadAnimate>().ShowSprite(false);
                        performingItem.GetComponent<ForageController>().multiplier = 0;
                        performingItem.GetComponent<ForageController>().suckLifeTime = true;
                        sucking = true;

                        performing = true;
                        pressed = false;

                        //send EatingControl to eventmanager
                        //pauses player
                        //sets performing to true
                        //
                    }
                    */
                
            }
        }

    }

    void checkReactivate()
    {
        if (performingItem != null)
        {
            if (performingItem.GetComponent<Crumb>() != null)
            {
               // licking = false;
            }
            /*
            if (performingItem.GetComponent<ForageController>() != null)
            {
                performingItem.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
                performingItem.GetComponent<gingerbreadAnimate>().ShowSprite(true);
                eating.SetActive(false);
                sucking = false;
                GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
                GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

                performingItem.GetComponent<ForageController>().multiplier = 1;
                performingItem.GetComponent<ForageController>().suckLifeTime = false;

                performingItem = null;
                performing = false;
                //send PutDown to eventmanager
                //unpauses player
            }
            */
        }
    }
    void FixedUpdate()
    {
        {
             
            // Make it move 10 meters per second instead of 10 meters per frame...
            translationX *= Time.deltaTime;
            translationY *= Time.deltaTime;
            Vector3 m_Input = new Vector3(translationX, translationY, 0);
            // Move translation along the object's z-axis
            rb.velocity = ( m_Input);


            // Rotate around our y-axis
        }
    }
    private void staminaLow()
    {
        //checks to see how low the stamina has gotten, and if so does not let subtract anymore
        if (stamina > 0)
        {
            sweating = false;
            stamina -= staminaLowerBy * Time.deltaTime;

        }
        else
        {
            staminaLower = false;

            if (currentlyPushing != 0)
            {
                sweating = true;
                if (currentID == 1)
                {
                    GM.FreezeObject(currentlyPushing);
                }

            }
        }
        GM.StaminaChange(stamina);

    }

    //increase stamina of player
    private void OnStaminaIncrease(float addStamina)
    {

        if (stamina < 1)
        {
            stamina += addStamina * Time.deltaTime;

        }
        GM.StaminaChange(stamina);

    }

    //when the player is pushing
    private void OnPlayerPushing(int id, int instanceID)
    {
//        print("OLL");
        currentlyPushing = instanceID;
        currentID = id;
        if (id == 1 && stamina > 0)
        {
            GM.UnFreezeObject(instanceID);
            speed = newSpeed / 2;
            staminaLower = true;
            staminaLowerBy = strength;
        }
        else if (id == 1 && stamina <= 0)
        {
            GM.FreezeObject(instanceID);
        }

        if (id == 2 && stamina > 0)
        {
            GM.UnFreezeObject(instanceID);
            speed = newSpeed / 1.5f;
            //staminaLower = true;
            staminaLowerBy = 0f;

        }
        else if (id == 2 && stamina <= 0)
        {
            GM.FreezeObject(instanceID);
        }

        if (id == 3 && stamina > 0)
        {
            //GM.UnFreezeObject(instanceID);
            speed = newSpeed / 1.5f;

        }
        else if (id == 3 && stamina <= 0)
        {
            GM.FreezeObject(instanceID);
        }


    }

    private void OnPlayerExit(int id, int instanceID, string stay)
    {
        if (stay != "staying")
        {
            currentlyPushing = 0;
        }
  
            staminaLower = false;

       



        GM.UnFreezeObject(currentlyPushing);
        //GM.FreezeObject(instanceID);

        speed = newSpeed;


    }
    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, contextRadius);

        Gizmos.color = Color.red;
    }
}
