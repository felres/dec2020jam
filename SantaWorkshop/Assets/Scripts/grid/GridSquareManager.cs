using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// public enum SquareType { INTRO, MAIN_MENU, LEVEL_SELECT, GAME, CREDITS }

public class GridSquareManager : MonoBehaviour
{
    public int wetAmount = 0;
    public static int maxWetAmount = 3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void ScanNeighbors()
    { 
        // Check neighbors and their states, change my own state
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<SpriteRenderer>().color = new Color(0, 0, (float)wetAmount/(float)maxWetAmount, 1);
    }
}
