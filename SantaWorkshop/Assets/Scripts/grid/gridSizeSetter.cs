using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gridSizeSetter : MonoBehaviour
{
    [SerializeField]
    private int gridSize = 8;
    [SerializeField]
    private GameObject balls;
    private float separation = 1.1f;
    // Empty 2d grid. array of arrays.
    private GameObject[,] grid;
    GameManager GM;

    

    // Start is called before the first frame update
    void Awake()
    {
        GM = GameManager.Instance;
        gridSize=GM.levelSize[GM.currLevel]; //passes in current levelSize
        print(gridSize);
        Debug.Log("Current game state when Awakes: " + GM.gameState);
    }

    void Start()
    {
        gridCreate();
    }

    private void gridCreate()
    {

        // make grid
        grid = new GameObject[gridSize,gridSize];

        for( int y = 0; y < gridSize; y++ )
        {
            for (int x = 0; x < gridSize; x++)
            {
                
                GameObject foo = Instantiate(balls, new Vector3(x * separation, - y * separation, 0), Quaternion.identity);
                print(foo);
                grid[y, x] = foo;
            }
        }
        print("The Length of the grid is "+ grid.GetLength(0)+ " by " +grid.GetLength(1) +"." );
    }

    [ContextMenu("Update Grid! - gridUpdate")]
    void gridUpdate()
    {
        GameObject[,] newGrid = new GameObject[gridSize, gridSize];
        
        for (int y = 0; y < gridSize; y++)
        {
            for (int x = 0; x < gridSize; x++)
            {
                newGrid[y, x] = Instantiate(balls, new Vector3(x * separation, -y * separation, 0), Quaternion.identity);

                // For now only wetness, a number 0 to 3. is calculated
                // Define variables for cardinal directions 
                int myWetness, wetnessN, wetnessE, wetnessS, wetnessW;
                GameObject currentSquare = grid[y, x];
                GridSquareManager cs = currentSquare.GetComponent<GridSquareManager>();
                myWetness = cs.wetAmount;
                //N
                wetnessN = 0;
                if (y > 0)
                    wetnessN = grid[y - 1, x].GetComponent<GridSquareManager>().wetAmount;
                //E
                wetnessE = 0;
                if (x < gridSize - 1)
                    wetnessE = grid[y, x + 1].GetComponent<GridSquareManager>().wetAmount;
                //S
                wetnessS = 0;
                if (y < gridSize - 1)
                    wetnessS = grid[y + 1, x].GetComponent<GridSquareManager>().wetAmount;
                //W
                wetnessW = 0;
                if (x > 0)
                    wetnessW = grid[y, x - 1].GetComponent<GridSquareManager>().wetAmount;

                // Make a change based on their neighbors water value
                if ((wetnessN + wetnessE + wetnessS + wetnessW) > myWetness)
                    newGrid[y, x].GetComponent<GridSquareManager>().wetAmount = Math.Min(myWetness + 1, GridSquareManager.maxWetAmount);
            }
        }

        for (int y = 0; y < gridSize; y++)
            for (int x = 0; x < gridSize; x++)
                Destroy(grid[y, x].gameObject);

        grid = newGrid;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
