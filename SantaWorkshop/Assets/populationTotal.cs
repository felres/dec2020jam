using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class populationTotal : MonoBehaviour
{
    EventManager GM;

    // Start is called before the first frame update
    void Awake()
    {
        GM = EventManager.Instance;
        GM.onGBMMaxChange += OnGBMMaxChange;

    }

    // Update is called once per frame
    private void OnGBMMaxChange(int change)
    {
        GetComponent<Text>().text = change.ToString() ;
        //+ "/"+GM.getMaxGBMAmount().ToString
    }
}

