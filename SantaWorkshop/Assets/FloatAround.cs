using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatAround : MonoBehaviour
{
    
    Vector2 floatingDirection;
    public float speed = 0.001f;

    // Start is called before the first frame update
    void Start()
    {
        floatingDirection = Random.insideUnitCircle;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += (Vector3)floatingDirection*speed;
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
