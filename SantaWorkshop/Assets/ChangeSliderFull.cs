using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChangeSliderFull : MonoBehaviour
{
    EventManager GM;

    int onSwitch = 0;
    int onSwitchSpeed = 0;

    private void Awake()
    {
        GM = EventManager.Instance;

        GM.OnFoodChange += OnFoodChange;

    }

    // Update is called once per frame
    private void OnFoodChange(float change)
    {
        GetComponent<Slider>().value = change;
    }
}