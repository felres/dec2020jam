 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaderFlicker : MonoBehaviour
{
    private float scanlineIntensityDefault;
    private int scanlineWidthDefault;
    private float shiftDefault;
    // Start is called before the first frame update
    void Start()
    {
        scanlineIntensityDefault = GetComponent<ShaderEffect_CRT>().scanlineIntensity;
        scanlineWidthDefault = GetComponent<ShaderEffect_CRT>().scanlineWidth;
        shiftDefault = GetComponent<ShaderEffect_CorruptedVram>().shift;
    }

    // Update is called once per frame
    void Update()
    {
        if( Random.Range(0,500) < 1f )
        {
            GetComponent<ShaderEffect_CRT>().scanlineIntensity = Random.Range(-scanlineIntensityDefault, scanlineIntensityDefault);
            GetComponent<ShaderEffect_CorruptedVram>().shift = Random.Range(-shiftDefault, shiftDefault);
        }
    }
}
