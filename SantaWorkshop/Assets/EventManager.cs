using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public delegate void OnFreezeObject(int instanceID);
public delegate void OnUnFreezeObject(int instanceID);
public delegate void OnStaminaIncrease(float add);
public delegate void OnGBMChange(int change);
public delegate void OnGBMMaxChange(int change);
public delegate void OnMainTextChange(string change);

public delegate void OnFoodChange(float change);
public delegate void OnPlayerPusherHandler(int id, int instanceID);
public delegate void OnReset();
public delegate void OnPlayerExitHandler(int id, int instanceID, string stay);
public delegate void OnPauseHandler(bool paused);
public delegate void OnStaminaChangeHandler(float change);



public class EventManager : MonoBehaviour
{

    private static EventManager instance= null;
    int crumbCount = 0;
    int gingerBreadCount = 0;
    int maxCrumbAmount = 500;
    int maxGBMAmount = 50;


    void Awake()
    {

        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);

        }
        else
        {
            //Destroy(gameObject);
        }
        Physics2D.IgnoreLayerCollision(0, 11);
        Physics2D.IgnoreLayerCollision(8, 11);
        Physics2D.IgnoreLayerCollision(0, 10);
        Physics2D.IgnoreLayerCollision(0, 7);




    }

    public static EventManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<EventManager>();
                if (instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "EventManager";
                    instance = go.AddComponent<EventManager>();
                    DontDestroyOnLoad(go);

                }

            }
            return instance;
        }

    }
    //pause event
    public event OnReset onReset;
    public void Reset()
    {
        crumbCount = 0;

        gingerBreadCount = 0;
        
        if (onReset!= null)
        {
            onReset();
        }
    }
    public event OnPauseHandler onPause;

    public void Pause(bool paused)
    {

        if (paused)
        {
            // pause the game/physic
            Time.timeScale = 0.0f;
        }
        else
        {
            // resume
            Time.timeScale = 1.0f;
        }
        if (onPause != null)
        {
            //show pause menu
        }
    }
    public void Speed(int speed)
    {

        if (speed ==0)
        {
            // pause the game/physic
            Time.timeScale = 1.0f;
        }
        else if (speed ==1)
        {
            // resume
            Time.timeScale = 2.0f;
        }
        else if (speed == 2)
        {
            // resume
            Time.timeScale = 3.0f;
        }
        if (onPause != null)
        {
            //show pause menu
        }
    }



    //event for pushing
    public event OnPlayerPusherHandler OnPlayerPusher;
    public void PushTriggerEnter(int id, int instanceID)
    {

        if (OnPlayerPusher != null)
        {

            OnPlayerPusher(id, instanceID);
        }
    }
    //event for pushing
    public event OnFoodChange OnFoodChange;
    public void FoodChange(float change)
    {

        if (OnFoodChange != null)
        {

            OnFoodChange(change);
        }
    }


    public event OnPlayerExitHandler onPlayerExit;
    public void PushTriggerExit(int id, int instanceID, string stay)
    {
        if (onPlayerExit != null)
        {
            onPlayerExit(id, instanceID, stay);
        }
    }


    public event OnStaminaChangeHandler onStaminaChange;
    public void StaminaChange(float amount)
    {
        if (onStaminaChange != null)
        {
            onStaminaChange(amount);
        }
    }


    public event OnFreezeObject onFreezeObject;
    public void FreezeObject(int instanceID)
    {
        if (onFreezeObject != null)
        {
            onFreezeObject(instanceID);
        }
    }
    public event OnUnFreezeObject onUnFreezeObject;
    public void UnFreezeObject(int instanceID)
    {
        if (onUnFreezeObject != null)
        {
            onUnFreezeObject(instanceID);
        }
    }

    public event OnStaminaIncrease onStaminaIncrease;
    public void StaminaIncrease(float stamina)
    {
        if (onStaminaIncrease != null)
        {
            onStaminaIncrease(stamina);
        }
    }
    public event OnGBMChange onGBMChange;
    public void GBMChange(int change)
    {
        if (onGBMChange != null)
        {
            onGBMChange(change);
        }
    }

    public event OnMainTextChange onMainTextChange;
    public void MainTextChange(string change)
    {
        if (onMainTextChange != null)
        {
            onMainTextChange(change);
        }
    }


    public void ChangeCrumbCount(int change)
    {
        crumbCount += change;
    }
    public int getCrumbCount()
    {
        return crumbCount;
    }

    public void ChangeGBMCount(int change)
    {

            gingerBreadCount += change;
            GBMChange(gingerBreadCount);
        if (gingerBreadCount == 0)
        {
            MainTextChange("Cookies went extinct... loading fail screen...");
            Invoke("TransitionFail", 2f);

        }
        if (gingerBreadCount + change > maxGBMAmount)
        {
            //end game
            GBMChange(gingerBreadCount);

            MainTextChange("Cookie Goal reached! Loading win screen...");
            Invoke("Transition", 2f);
        }

    }
    public void TransitionFail()
    {
        Time.timeScale = 1.0f;

        SceneManager.LoadScene(1);
    }
    public void Transition()
    {
        Time.timeScale = 1.0f;

        SceneManager.LoadScene(2);
    }
    public int getGBMCount()
    {
        return gingerBreadCount;
    }
    public int getMaxCrumbAmount()
    {
        return maxCrumbAmount;
    }
    public int getMaxGBMAmount()
    {
        return maxGBMAmount;
    }
    public event OnGBMMaxChange onGBMMaxChange;

    public void ChangeMaxGBMAmount(int max)
    {
        maxGBMAmount = max ;
        MaxGBMChangeAmount(max);
    }
    public void MaxGBMChangeAmount(int max)
    {
        if (onGBMMaxChange!= null)
        {
            onGBMMaxChange(max);

        }
    }

}



