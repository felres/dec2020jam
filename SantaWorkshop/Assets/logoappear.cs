using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class logoappear : MonoBehaviour
{
    public GameObject instr;
    // Start is called before the first frame update
    void Awake()
    {
        Invoke("TurnOff", 4f);
    }
    void TurnOff()
    {
        this.gameObject.SetActive(false);
        instr.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {
        GetComponent<Image>().fillAmount = Time.time;
    }
}


