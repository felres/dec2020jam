using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaAnimate : MonoBehaviour
{
    Animator anim;
    public Animator animChild;
    // Start is called before the first frame update
    void Start()
    {

    }
    private void Awake()
    {
        anim = GetComponent<Animator>();

    }
    // Update is called once per frame
    public void TriggerStopShove()
    {
        GetComponent<PlayerController>().SetShove(false);
    }
    public void ChangeAnimation(string state, bool pushing, bool foodcoma, bool shoving, bool licking, string lastDir)
    {
        if (foodcoma)
        {
            anim.Play("food_coma");

        }
        else if (licking)
        {
            switch (state)
            {
                case "forward":
                    //move forward
                    anim.Play("left_lick_floor_walk");
                    animChild.Play("left_lick_floor_walk");

                    break;
                case "left":
                    //move left
                    anim.Play("left_lick_floor_walk");
                    animChild.Play("left_lick_floor_walk");

                    break;
                case "right":
                    //move right
                    anim.Play("right_lick_floor_walk");
                    animChild.Play("right_lick_floor_walk");

                    break;
                case "up":
                    //move up
                    anim.Play("right_lick_floor_walk");
                    animChild.Play("right_lick_floor_walk");

                    break;
                default:
                    anim.Play("lick_floor_idle");
                    animChild.Play("lick_floor_idle");
                    break;


            }
        }
       else
        {
            string add = "";
            string myState = state;
            if (pushing)
            {
                add = "push_";
            }
            if (shoving)
            {
                myState = "idle"; 
                add = "shove_";
            }
            


            switch (myState)
            {
                case "forward":
                    //move forward
                    anim.Play("forward_" + add + "walk");
                    animChild.Play("forward_" + add + "walk");

                    break;
                case "left":
                    //move left
                    anim.Play("left_" + add + "walk");
                    animChild.Play("left_" + add + "walk");

                    break;
                case "right":
                    //move right
                    anim.Play("right_" + add + "walk");
                    animChild.Play("right_" + add + "walk");

                    break;
                case "up":
                    //move up
                    anim.Play("up_" + add + "walk");
                    animChild.Play("up_" + add + "walk");

                    break;
                default:
                    performIdle(lastDir, add);
                    break;
            }

        }
        
        
    }
    public void performIdle(string lastDir, string add)
    {
        //print(lastDir + add + "idle");

                //move forward
                anim.Play(lastDir +"_"+add+"idle");
        string myAdd = "";
                animChild.Play(lastDir + "_" + myAdd + "idle");


        }
    
    

}
