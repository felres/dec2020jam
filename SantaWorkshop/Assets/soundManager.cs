using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
       // StopMe();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlaySound(AudioClip ad)
    {
        GetComponent<AudioSource>().PlayOneShot(ad);
    }
    public void PlayMe()
    {
        GetComponent<AudioSource>().enabled = true;
    }
    public void StopMe()
    {
        GetComponent<AudioSource>().enabled = false;
    }
}
