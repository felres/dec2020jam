using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class UIManager : MonoBehaviour
{
    EventManager GM;

    int onSwitch = 0;
    int onSwitchSpeed = 0;
    public CreditManager cm;
    public Text txto;
    private void Awake()
    {
        GM = EventManager.Instance;

    }
    public void HandlePauseCommand()
    {
        onSwitch += 1;
        cm.OffCredit();
        GM.Pause(onSwitch % 2 == 1);
    }
    public void HandleSpeedCommand()
    {
        onSwitch += 1;
        string sp = "";
        if (onSwitch%3 == 0)
        {
            sp =  ">";
        }
        if (onSwitch % 3 == 1)
        {
            sp = ">>";
        }
        if (onSwitch % 3 == 2)
        {
            sp = ">>>";
        }
        txto.text = "Speed" + sp;
        GM.Speed(onSwitch % 3);
    }

    public void Reset()
    {
        SceneManager.LoadScene(0);

    }
    public void HandleAddStamina()
    {

        print("Jullo");

        GM.StaminaIncrease(.2f);

    }

}
