using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class staminaChanger : MonoBehaviour
{
    EventManager GM;

    // Start is called before the first frame update
    void Awake()
    {
        GM = EventManager.Instance;
        GM.onStaminaChange += OnStaminaChange;

    }

    // Update is called once per frame
    private void OnStaminaChange(float change)
    {
        GetComponent<RectTransform>().eulerAngles = new Vector3(0,0, GameManager.map(change,0,1,40,-40)+40); 
    }
}

