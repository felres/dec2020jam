using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpdateText : MonoBehaviour
{
    EventManager GM;

    // Start is called before the first frame update
    void Awake()
    {
        GM = EventManager.Instance;
        GM.onMainTextChange += OnUpdateChange;


    }

    // Update is called once per frame
    private void OnUpdateChange(string change)
    {
        GetComponent<Text>().text = change;
    }
}
