using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToSpawn : MonoBehaviour
{
    public GameObject spawn;
     Camera mainCam;
    // Start is called before the first frame update
    void Start()
    {
        mainCam = Camera.main;
    }
    void OnMouseOver()
    {
        Debug.Log("HLE!");

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 camPosition = mainCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
            camPosition = new Vector3(camPosition.x, camPosition.y, 0);
            Instantiate(spawn, camPosition, transform.rotation);
        }
    }
}
