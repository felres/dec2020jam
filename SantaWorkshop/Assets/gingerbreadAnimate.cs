using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gingerbreadAnimate : MonoBehaviour
{
    [SerializeField]
    GameObject animatedChild;
    [SerializeField]
    GameObject animatedBoots;
    [SerializeField]
    GameObject animatedDough;
    float myDirection;
    bool bootState = true;

    private void Awake()
    {
    }
    // Start is called before the first frame update
    void Start()
    {

    }
    public void ChangeAngle(Vector2 desiredDirection)
    {
        myDirection = GameManager.DirectionToAngle(desiredDirection); //changes direction
        float myDirectionTest = GameManager.AngleTo(Vector2.right, desiredDirection);
        string age = "young";
        //print(myDirectionTest + "+" + myDirection);
        if (GetComponent<ForageController>().lifespan < 50)
        {
            age = "old";
        }
        if (myDirection < -180 + 45 || myDirection > 180 - 45)
        {
            animatedChild.GetComponent<Animator>().Play("left_"+age);
            if (!bootState)
            {
                animatedDough.GetComponent<Animator>().Play("forward_dough");
            }
            else
            {
                animatedBoots.GetComponent<Animator>().Play("forward_boots");
            }


        }
        else if (myDirection > -180 + 45 && myDirection < -90 + 45)
        {
            animatedChild.GetComponent<Animator>().Play("forward_" + age);
            if (!bootState)
            {
                animatedDough.GetComponent<Animator>().Play("forward_dough");
            }
            else
            {
                animatedBoots.GetComponent<Animator>().Play("forward_boots");
            }


        }
        else if (myDirection < 90 - 45 && myDirection > -90 + 45)
        {
            animatedChild.GetComponent<Animator>().Play("right_" + age);
            if (!bootState)
            {
                animatedDough.GetComponent<Animator>().Play("forward_dough");
            }
            else
            {
                animatedBoots.GetComponent<Animator>().Play("forward_boots");
            }


        }
        else if (myDirection < 180 - 45 && myDirection > 90 - 45)
        {
            animatedChild.GetComponent<Animator>().Play("up_" + age);
            if (!bootState)
            {
                animatedDough.GetComponent<Animator>().Play("up_dough");
            }
            else
            {
                animatedBoots.GetComponent<Animator>().Play("up_boots");
            }


        }
        animatedChild.transform.localEulerAngles = -transform.localEulerAngles;

    }

    public void ShowSprite(bool show)
    {
        animatedChild.SetActive(show);

        if (bootState == true)
        {
            animatedDough.SetActive(false);
            animatedBoots.SetActive(show);

        }

        if (bootState == false)
        {
            animatedDough.SetActive(show);
            animatedBoots.SetActive(false);

        }

    }
    public void ChangeState(int state)
    {
        if (state == 0)
        {
            animatedBoots.SetActive(true);
            animatedDough.SetActive(false);
            bootState = true;

        }
        else
        {
            animatedBoots.SetActive(false);
            animatedDough.SetActive(true);
            bootState = false;



        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
