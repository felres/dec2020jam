using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crumb : MonoBehaviour
{
    public float creationTime = 0;
    public float lifespan = 100f;
    public float breadcrumbEvaporationTime = .1f;
    public float facingAngle;
    EventManager GM;
    // Start is called before the first frame update
    void Awake()
    {
        creationTime = Time.time;
        GM = EventManager.Instance;
    }

    public float getValueOld()
    {
        float lifetime = Time.time - creationTime;
        float evaporateAmount = Mathf.Max(1, lifetime / breadcrumbEvaporationTime);
        float lifePercentage = 1 - evaporateAmount;
        print("returned: " + lifePercentage);
        return lifePercentage;
    }

    public float getValue()
    {
        float lifetime = Time.time - creationTime;
        if (lifetime >= lifespan)
        {
            GM.ChangeCrumbCount(-1);

            Destroy(this.gameObject);
        }
       // Color col = GetComponent<SpriteRenderer>().color;
        //col.a = 1- lifetime / lifespan;
        //GetComponent<SpriteRenderer>().color = col;
        return -lifetime;
    }

    // Update is called once per frame
    void Update()
    {
        getValue();
    }

    
}
