using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followObject : MonoBehaviour
{
    public GameObject follow;
    // Start is called before the first frame update
    void Awake()
    {
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = new Vector3(follow.transform.position.x, follow.transform.position.y, transform.position.z);
    }
}
