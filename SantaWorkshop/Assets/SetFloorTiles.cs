using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFloorTiles : MonoBehaviour
{

    public bool initial = false;
    public GameObject tileObject;
    [SerializeField]
    private int gridSize = 8;
    public float tileLength;
    // Start is called before the first frame update
    void Awake()
    {
        tileLength = tileObject.GetComponent<SpriteRenderer>().bounds.size.x;
        float tileLengthFixed = tileLength - 0.0f;// 3f;
        if (initial)
        {
            for (int y = 0; y < gridSize; y++)
            {
                for (int x = 0; x < gridSize; x++)
                {
                    GameObject foo = Instantiate(tileObject, transform.position + new Vector3(x * tileLengthFixed, y * -tileLengthFixed, 0), Quaternion.identity);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
