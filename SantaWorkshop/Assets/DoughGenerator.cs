using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoughGenerator : MonoBehaviour
{
    public GameObject spawn;
    public float alarmRate = 2;
    private float timer = 0;
    private SpriteRenderer sr;
    int maxCookies = 20;
    int currCookies = 0;
    void Awake()
    {
        maxCookies = EventManager.Instance.getMaxGBMAmount();
        sr = GetComponent<SpriteRenderer>();
    }
    void Update()
    {
        float withinF = Time.deltaTime;
        if (GameManager.within((Time.time % alarmRate), 0, withinF))
        {

            Instantiate(spawn, (Vector2)transform.position + (Random.insideUnitCircle*0.4f) + new Vector2(0, -sr.bounds.size.y/2), transform.rotation);
            currCookies += 1;
        }
    }
}
