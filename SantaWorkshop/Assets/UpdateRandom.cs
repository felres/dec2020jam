using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpdateRandom : MonoBehaviour
{
    [SerializeField]
    public float changeRate = 5;
    float changer = 0;
    float change = 0;
    static float t = 0.0f;
    public float value = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float withinF = Time.fixedDeltaTime;

        if (value< changer)
        {
            value += 0.5f * Time.deltaTime;

        }
        else 
        {
            value -= 0.5f * Time.deltaTime;

        }
        t += Time.deltaTime * 0.5f;
        //value =Mathf.Lerp(change, changer, t);
        GetComponent<Slider>().value = value;


        if (t > 1.0f)
        {
            float temp = changer;
            changer = Random.Range(.1f, 1f);

//            print(changer + ", " + change);

            t = 0.0f;
        }
        // .. and increase the t interpolater

        // now check if the interpolator has reached 1.0
        // and swap maximum and minimum so game object moves
        // in the opposite direction.

    }
}
