using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChangeHungerLevel : MonoBehaviour
{
    EventManager GM;



    private void Awake()
    {
        GM = EventManager.Instance;

        GM.OnFoodChange += OnFoodChangeText;

    }

    // Update is called once per frame
    private void OnFoodChangeText(float change)
    {
        if (change > .9f)
        {
            GetComponent<Text>().text = "status: food coma";
        }
        else if (change > .1f)
        {
            GetComponent<Text>().text = "status: hungry";

        }
        else
        {
            GetComponent<Text>().text = "status: starving";

        }
    }
}