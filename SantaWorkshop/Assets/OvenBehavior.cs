using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OvenBehavior : MonoBehaviour
{
    [SerializeField]
    private GameObject gingerbreadPrefab;
    [SerializeField]
    private int startingGingerbreadAmount = 10;
    private int doughCount = 0;
    [SerializeField]
    private int creationMinimum = 3;
    [SerializeField]
    private int maxGBMAmount = 3;
    EventManager GM;
    bool begin = true;
    public soundManager sm;
    public AudioClip bing;
    // Start is called before the first frame update
    void Start()
    {
        GM = EventManager.Instance;
        GM.onReset += Reset;
        
        for (int i = 0; i < startingGingerbreadAmount; i++)
        {
            createGingerbread();

        }
        GM.ChangeMaxGBMAmount(maxGBMAmount);
        sm.PlaySound(bing);

        begin = false;
        //GM.ChangeGBMCount(startingGingerbreadAmount);

    }

    public void Reset()
    {
        for (int i = 0; i < startingGingerbreadAmount; i++)
        {
            createGingerbread();

        }
        GM.ChangeMaxGBMAmount(maxGBMAmount);

    }

    public void increaseDoughCount()
    {
        if (doughCount != 0)
        {
            Invoke("AnimateOven", 0);
        }

        doughCount++;

            Invoke("AnimateOven", 0);
        
        if (doughCount >= creationMinimum)
        {
            doughCount -= creationMinimum;
            createGingerbread();
        }


    }


    private void AnimateOven()
    {
        GetComponent<OvenAnimate>().ChangeAnimation(doughCount);
    }

    private void createGingerbread()
    {
        GameObject gb = Instantiate(gingerbreadPrefab, transform.position, transform.rotation);
        gb.GetComponent<ForageController>().nest = transform.position;
        gb.GetComponent<ForageController>().my_oven = this.gameObject;
        GM.MainTextChange("A Gingerbread Man is Born!");
        if (begin == false)
        {
            sm.PlaySound(bing);
        }
        GM.ChangeGBMCount(1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
