using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChangeImage : MonoBehaviour
{
    EventManager GM;

    public Sprite img1;
    public Sprite img2;
    public Sprite img3;

    private void Awake()
    {
        GM = EventManager.Instance;

        GM.OnFoodChange += OnFoodChangeImg;

    }

    // Update is called once per frame
    private void OnFoodChangeImg(float change)
    {
        if (change > .9f)
        {
            GetComponent<Image>().sprite = img1;
        }
        else if (change> .1f)
        {
            GetComponent<Image>().sprite = img2;

        }
        else
        {
            GetComponent<Image>().sprite = img3;

        }
    }
}

